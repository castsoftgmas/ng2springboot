/// <vs BeforeBuild='preBuild' AfterBuild='postBuild' Clean='clean' />
'use strict';

/*
 *    Using webpack-stream to run webpack from gulp:
 *        https://webpack.github.io/docs/usage-with-gulp.html
 *        https://github.com/shama/webpack-stream
 *
 *    Useful info:
 *        http://www.davepaquette.com/archive/2014/10/08/how-to-use-gulp-in-visual-studio.aspx
 *        http://docs.asp.net/en/latest/client-side/using-gulp.html
 *        https://github.com/gulpjs/gulp/blob/master/docs/recipes/README.md
 *
 *	  Previously used plugins
 *		var embed = require('gulp-angular-embed-templates');    // gulp-angular-embed-templates removes some quotes from the template which causes ng2 to blowup when parsing the template! Namely when parsing hrefs b/c the forward slash is a problem
 *		var cssmin = require('gulp-cssmin');  // seems to be superceded by gulp-clean-css
 *
 */

var gulp = require('gulp');
var del = require('del');
var inlineNg2Template = require('gulp-inline-ng2-template');
var debug = require('gulp-debug');
var sass = require('gulp-sass');
var minifyCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var prefixer = require('gulp-autoprefixer');
var runSequence = require('run-sequence');
var webpack = require('webpack-stream');

/*
 *
 *      CLEAN
 *
 */
gulp.task('cleanScripts', function () {
    return del(['./app/scripts/dist']);
});

gulp.task('cleanApp', function () {
    return del(['./app/**/*.js']);
});

gulp.task('cleanMaps', function () {
    return del(['./app/**/*.js.map']);
});

gulp.task('cleanWebpackFiles', function () {
    return del(['./tmpWebpackFiles']);
});

gulp.task('cleanEmbeddedCSS', function () {
    return del(['./app/**/*.css']);
});

gulp.task('cleanBundles', function() {
   return del(['../webapp/resources/*.*'], {force: true});
});


/**
 No global SASS files yet

 gulp.task('cleanGlobalCSS', function () {
	return del(['./client/css/dist']);
});
 */
gulp.task('cleanAllJs', ['cleanScripts', 'cleanApp', 'cleanMaps', 'cleanWebpackFiles', 'cleanBundles']);

/*
 *
 *      SASS COMPILATION
 *
 */
gulp.task('compileEmbeddedSASSandAddPrefixes', function () {
    return gulp.src(['app/**/*.scss'])
        .pipe(debug({ title: 'compileEmbeddedSASSandAddPrefixes' }))
        .pipe(sass())
        .pipe(prefixer({ browsers: ['last 3 versions'] }))
        .pipe(gulp.dest('./app'));
});

/**
 No global SASS files yet

 gulp.task('compileGlobalSASSandAddPrefixes', function () {
	return gulp.src(['client/css/*.scss'])
      .pipe(debug({ title: 'compileGlobalSASSandAddPrefixes' }))
      .pipe(sass())
      .pipe(prefixer({ browsers: ['last 3 versions'] }))
      .pipe(gulp.dest('./client/css/dist'));
});
 */


/*
 *
 *      CSS MINIFICATION
 *
 */
gulp.task('minifyEmbeddedCSS', function () {
    return gulp.src(['app/**/*.css', '!app/**/*.min.css'])    // minify all css files EXCEPT for those already minified
        .pipe(debug({ title: 'minifyEmbeddedCSS' }))
        .pipe(minifyCss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('app'));
});

/**
 No global SASS files yet

 gulp.task('minifyGlobalCSS', function () {
	return gulp.src(['client/css/dist/*.css', '!client/css/dist/*.min.css'])    // minify all css files EXCEPT for those already minified
      .pipe(debug({ title: 'minifyGlobalCSS' }))
      .pipe(minifyCss())
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('client/css/dist'));
});
 */

/*
 *
 *      EMBED TEMPLATES
 *
 */
gulp.task('embedTemplates', function () {
    return gulp.src('app/**/*.js')
        .pipe(debug({ title: 'Component - inline and write to tmpWebpackFiles' }))
        .pipe(inlineNg2Template({ base: 'app', target: 'es5', removeLineBreaks: true, useRelativePaths: true }))
        .pipe(gulp.dest('./tmpWebpackFiles'));

});


/*
 *
 *      WEBPACK
 *
 */
gulp.task('webpack', function () {
    return gulp.src(['./tmpWebpackFiles/main.js'])   // src param just kicks off the process. Webpack will use entry property
        .pipe(debug({ title: 'Webpack - bundle' }))
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('./scripts'));
});



/*
 *
 *      BUILD COMMANDS
 *
 */

gulp.task('clean', ['cleanEmbeddedCSS', 'cleanAllJs']);	// 'cleanGlobalCSS',

gulp.task('preBuild', function (callback) {
    runSequence(
        ['cleanEmbeddedCSS', 'cleanAllJs'],				// 'cleanGlobalCSS',
        'compileEmbeddedSASSandAddPrefixes',
        //'compileGlobalSASSandAddPrefixes',
        'minifyEmbeddedCSS',
        //'minifyGlobalCSS',
        callback);
});

gulp.task('postBuild', function (callback) {
    runSequence(
        'embedTemplates',
        'webpack',
        callback);
});


/** This is for DEVs only. Run this after changing ts, scss, or html, files (i.e. no C# files)
 *  It will build the angular2 code quicker than a project rebuild.
 *  It takes care of sass, minification, embedding, etc.
 *  It assumes a project build has already occurred.
 */
gulp.task('build', function (callback) {
    runSequence(
        'compileEmbeddedSASSandAddPrefixes',
        //'compileGlobalSASSandAddPrefixes',
        'minifyEmbeddedCSS',
        //'minifyGlobalCSS',
        'embedTemplates',
        'webpack',
        callback);
});
