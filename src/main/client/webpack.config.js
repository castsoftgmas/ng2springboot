// NOTE: this file is kicked off by a gulpfile task.

var webpack = require('webpack');
var path = require('path');
// Dependency: This file uses js files, hence the typeScript compilation must have occurred before webpack is invoked.


module.exports = {
    context: path.join(__dirname, ''),

    entry: {
        "app": "./tmpWebpackFiles/main"
    },

    devtool: 'source-map',      // create source maps

    module: {
        loaders: [{
            test: /\.css$/,
            loader: 'style-loader!css-loader'
        }, {
            test: /\.html$/,
            loader: 'raw'
        }, {
            test: /\.(png|jpg|gif)$/,
            loader: 'url?limit=10000'
        }]
    },

    output: {
        path: path.join(__dirname, '/scripts'),
        filename: "../../webapp/resources/[name].bundle.js"
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin( {	// create a common bundle of code shared between entry point apps
            name: "common",
            filename: "../../webapp/resources/common.bundle.js"
        })
    ]

};