import { NgModule }      from '@angular/core';

import {AppComponent} from "./app.component";
import {TradingEntitiesMainModule} from "./trading-entities/trading-entities-main.module";

@NgModule({
    imports:      [ TradingEntitiesMainModule ],
    declarations: [ AppComponent ],
    bootstrap:    [ AppComponent ]
})

export class AppModule { }