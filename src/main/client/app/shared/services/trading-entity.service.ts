import {Injectable, EventEmitter}    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { TradingEntity } from './../trading-entity/trading-entity.bean';

@Injectable()
export class TradingEntityService {

    updated: EventEmitter<string> = new EventEmitter<string>();

    private headers = new Headers({'Content-Type': 'application/json'});
    private tradingEntitiesUrl = '/username/tradingentities';  // URL to web api

    constructor(private http: Http) { }

    getTradingEntities(): Promise<TradingEntity[]> {
        return this.http.get(this.tradingEntitiesUrl)
            .toPromise()
            .then(response => response.json() as TradingEntity[])
            .catch(this.handleError);
    }

    getTradingEntityForId(id: number): Promise<TradingEntity> {
        return this.http.get(this.tradingEntitiesUrl + '/' + id)
            . toPromise()
            .then(response => response.json() as TradingEntity)
            .catch(this.handleError);
    }

    getTradingEntity(url: string): Promise<TradingEntity> {
        return this.http.get(url)
            . toPromise()
            .then(response => response.json() as TradingEntity)
            .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        const url = `${this.tradingEntitiesUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    addTradingEntity(name:string, description:string, displayName:string): Promise<any> {
        return this.http
            .post(this.tradingEntitiesUrl, JSON.stringify({name: name, description: description, displayName: displayName}),
                {headers: this.headers})
            .toPromise()
            .then(res => {
                let newUrl = res.headers.get('location');
                this.updated.emit(newUrl);
                return newUrl;
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}