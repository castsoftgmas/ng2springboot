"use strict";
var trading_entity_1 = require('./trading-entity');
describe('TradingEntity', function () {
    it('has name', function () {
        var te = new trading_entity_1.TradingEntity('11', 'TE1', 'TE1 description', null, null, null, null);
        expect(te.name).toBe('TE1');
    });
    it('has id', function () {
        var te = new trading_entity_1.TradingEntity('1', 'Super Cat', null, null, null, null, null);
        expect(te.id).toBe('1');
    });
});
//# sourceMappingURL=trading-entity.spec.js.map