import { Account } from './../account/account.bean';
import { TradingEntityStatus } from './trading-entity-status';

export class TradingEntity {
    constructor(
        public id: string,
        public name: string,
        public displayName: string,
        public description: string,
        public status: TradingEntityStatus,
        public accounts: [Account],
        public counterparties: [TradingEntity]
    ){}

}