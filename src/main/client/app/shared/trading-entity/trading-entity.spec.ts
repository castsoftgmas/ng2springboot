import { TradingEntity } from './trading-entity.bean';

describe('TradingEntity', () => {
    it('has name', () => {
        const te = new TradingEntity('11', 'TE1', 'TE1 description', null, null, null, null);
        expect(te.name).toBe('TE1');
    });

    it('has id', () => {
        const te = new TradingEntity('1', 'Super Cat', null, null, null, null, null);
        expect(te.id).toBe('1');
    });

});