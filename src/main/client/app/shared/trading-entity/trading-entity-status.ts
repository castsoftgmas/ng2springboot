export class TradingEntityStatus {
    APPROVED: TradingEntityStatus;
    SUSPENDED: TradingEntityStatus;
    PENDING: TradingEntityStatus;
}