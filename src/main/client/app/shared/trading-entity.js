"use strict";
var TradingEntity = (function () {
    function TradingEntity(id, name, displayName, description, status, accounts, counterparties) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.description = description;
        this.status = status;
        this.accounts = accounts;
        this.counterparties = counterparties;
    }
    return TradingEntity;
}());
exports.TradingEntity = TradingEntity;
//# sourceMappingURL=trading-entity.js.map