/*
 This file is essentially for webpack
 List all imports and specs below
 */
import 'core-js/es6';
import 'core-js/es7/reflect';

import 'core-js/client/shim';
import 'reflect-metadata';
import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';


import { TestBed, async } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

/*
 https://angular.io/docs/ts/latest/guide/webpack.html
 */

TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());

// TODO: loop through specs and import them all here so that we don't need a separate specs.bundle
//
import "./shared/trading-entity.spec";
import "./trading-entities/trading-entities-table/trading-entities-table.component.spec";
