import { NgModule }      from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";

import { DataTableModule, SharedModule } from 'primeng/primeng';

import { TradingEntitiesMainComponent } from "./trading-entities-main.component";
import { TradingEntitiesTableComponent } from "./trading-entities-table/trading-entities-table.component";
import { TradingEntityComponent } from "./trading-entity/trading-entity.component";

import 'primeng/primeng';

import 'primeng/resources/themes/aristo/theme.css';
// import 'font-awesome/css/font-awesome.min.css';
import 'primeng/resources/primeng.css';
import {TradingEntityService} from "../shared/services/trading-entity.service";

@NgModule({
    imports:      [ BrowserModule, HttpModule, DataTableModule, SharedModule ],
    providers:    [ TradingEntityService ],
    declarations: [ TradingEntitiesMainComponent, TradingEntitiesTableComponent, TradingEntityComponent ],
    exports:      [ TradingEntitiesMainComponent, TradingEntitiesTableComponent ]
})

export class TradingEntitiesMainModule { }

