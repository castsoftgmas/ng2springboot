import { ComponentFixture, TestBed, fakeAsync, tick, discardPeriodicTasks } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import {DebugElement, EventEmitter} from "@angular/core";

import {TradingEntitiesTableComponent} from "./trading-entities-table.component";
import {DataTableModule} from "primeng/components/datatable/datatable";
import {TradingEntityService} from "../../shared/services/trading-entity.service";
import {TradingEntity} from "../../shared/trading-entity/trading-entity.bean";

const mockTradingEntities:[TradingEntity] = [
    { id: '1',
        name: 'name1',
        displayName: 'displayName1',
        description: 'description1'
    } as TradingEntity,
    {
        id: '2',
        name: 'name2',
        displayName: 'displayName2',
        description: 'description2'
    } as TradingEntity
]

describe('TradingEntitiesTableComponent', ()=> {

    let mockTradingEntityService: TradingEntityService, tradingEntityService: TradingEntityService;
    let fixture: ComponentFixture<TradingEntitiesTableComponent>;
    let tradingEntitiesTableComponent: TradingEntitiesTableComponent;
    let eventEmitter = new EventEmitter<string>();

    beforeEach(() => {

        mockTradingEntityService = {
            getTradingEntities: null,
            getTradingEntityForId: null,
            getTradingEntity: null,
            delete: null,
            addTradingEntity: null
        } as TradingEntityService;

        mockTradingEntityService.updated = eventEmitter;
       TestBed.configureTestingModule({
          declarations: [TradingEntitiesTableComponent],
           providers: [
               {provide : TradingEntityService, useValue: mockTradingEntityService }
           ],
           imports: [
               DataTableModule
           ]
       });
        fixture = TestBed.createComponent(TradingEntitiesTableComponent);
        tradingEntitiesTableComponent = fixture.componentInstance;
        tradingEntityService = fixture.debugElement.injector.get(TradingEntityService);
        spyOn(tradingEntityService, 'getTradingEntities').and.returnValue(Promise.resolve(
            mockTradingEntities
        ));
    });

    it('should create TradingEntitiesTable component', () => {
       expect(tradingEntitiesTableComponent).toBeDefined();
    });

    it('should display table header', () => {
        fixture.detectChanges();
        let de = fixture.debugElement.query(By.css('header'));
        let el = de.nativeElement;
        let content = el.textContent;
        expect(content).toContain('List of Trading Entities');
    });

    it('should display table footer', () => {
        fixture.detectChanges();
        let de = fixture.debugElement.query(By.css('footer'));
        let el = de.nativeElement;
        let content = el.textContent;
        expect(content).toContain('Choose from the list');
    });

    it('should display table', () => {
        fixture.detectChanges();
        let de = fixture.debugElement.query(By.css('table'));
        expect(de).toBeDefined();
    });

    it('should display 2 table rows', () => {
        let de: DebugElement[];
        fixture.detectChanges();
        de = fixture.debugElement.queryAll(By.css('tr'));
        expect(de.length).toBe(2);
    });

    it('should display 4 table column headers', () => {
        let de: DebugElement[];
        fixture.detectChanges();
        de = fixture.debugElement.queryAll(By.css('thead'));
        expect(de.length).toBe(1);
        let htmlTR = <HTMLBaseElement> de[0].children[0].nativeElement;
        expect(htmlTR.tagName).toBe('TR');
        expect(htmlTR.children.length).toBe(4);
        expect(htmlTR.children[0].tagName).toBe('TH');
        expect(htmlTR.children[0].textContent.trim()).toBe('ID');
        expect(htmlTR.children[1].textContent.trim()).toBe('Name');
        expect(htmlTR.children[2].textContent.trim()).toBe('Description');
        expect(htmlTR.children[3].textContent.trim()).toBe('DisplayName');
    });

    it('should display json content from service', fakeAsync(() => {
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        let de = fixture.debugElement.query(By.css('tbody'));
        let htmlTR1 = <HTMLBaseElement> de.children[0].nativeElement;
        expect(htmlTR1.tagName).toBe('TR');
        expect(htmlTR1.children.length).toBe(4);
        expect(htmlTR1.children[0].tagName).toBe('TD');
        expect(htmlTR1.children[0].textContent.trim()).toContain('1');
        expect(htmlTR1.children[1].tagName).toBe('TD');
        expect(htmlTR1.children[1].textContent.trim()).toContain('name1');
        expect(htmlTR1.children[2].tagName).toBe('TD');
        expect(htmlTR1.children[2].textContent.trim()).toContain('description1');
        expect(htmlTR1.children[3].tagName).toBe('TD');
        expect(htmlTR1.children[3].textContent.trim()).toContain('displayName1');
        let htmlTR2 = <HTMLBaseElement> de.children[1].nativeElement;
        expect(htmlTR2.tagName).toBe('TR');
        expect(htmlTR2.children[0].tagName).toBe('TD');
        expect(htmlTR2.children[0].textContent.trim()).toContain('2');
        expect(htmlTR2.children[1].tagName).toBe('TD');
        expect(htmlTR2.children[1].textContent.trim()).toContain('name2');
        expect(htmlTR2.children[2].tagName).toBe('TD');
        expect(htmlTR2.children[2].textContent.trim()).toContain('description2');
        expect(htmlTR2.children[3].tagName).toBe('TD');
        expect(htmlTR2.children[3].textContent.trim()).toContain('displayName2');
    }));

    it('should use DataTable directive', () => {
        let de = fixture.debugElement.query(By.directive(DataTableModule));
        expect(de).toBeDefined();
    });
});
