import {Component, OnInit} from '@angular/core';

import {TradingEntity} from "../../shared/trading-entity/trading-entity.bean";
import {TradingEntityService} from "../../shared/services/trading-entity.service";

@Component({
    selector: 'trading-entities-table',
    templateUrl: 'trading-entities-table.component.html',
    styleUrls: ['trading-entities-table.component.css']
})

export class TradingEntitiesTableComponent implements OnInit {
    tradingEntities: TradingEntity[];
    selectedTE : TradingEntity;

    constructor(
        private tradingEntityService: TradingEntityService
    ) {}

    getTradingEntities(): void {
        this.tradingEntityService.getTradingEntities()
            .then(tradingEntitities => this.tradingEntities = tradingEntitities);
    }

    add(name: string, description: string, displayName: string) {
        this.tradingEntityService.addTradingEntity(name, description, displayName)
            .then(teUrl => {
                this.tradingEntityService.getTradingEntity(teUrl).then(te =>
                {
                    this.tradingEntities.push(te);
                    this.selectedTE = null;
                })
            });
    }

    delete(te: TradingEntity): void {
        this.tradingEntityService
            .delete(te.id)
            .then(() => {
                this.tradingEntities = this.tradingEntities.filter(h => h !== te);
                if (this.selectedTE === te) { this.selectedTE = null; }
            });
    }

    ngOnInit(): void {
        this.getTradingEntities();
        this.tradingEntityService.updated.subscribe(
            () => {this.getTradingEntities();}
        )
    }

    onSelect(te: TradingEntity): void {
        this.selectedTE = te;
    }

    gotoDetail(): void {
        // this.router.navigate(['/detail', this.selectedTE.id]);
    }
}