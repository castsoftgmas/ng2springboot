import {Component, OnInit} from '@angular/core';

import {TradingEntityService} from "../../shared/services/trading-entity.service";

@Component({
    selector: 'trading-entity',
    templateUrl: 'trading-entity.component.html',
    styleUrls: ['trading-entity.component.css']
})
export class TradingEntityComponent implements OnInit {

    constructor(
        private tradingEntityService: TradingEntityService
    ) {}

    ngOnInit(): void {
    }

    add(name: string, description: string, displayName: string) {
        this.tradingEntityService.addTradingEntity(name, description, displayName);
    }
}