// Karma configuration
// See:
// https://egghead.io/lessons/tools-use-karma-for-unit-testing-with-webpack
// http://mike-ward.net/2015/09/07/tips-on-setting-up-karma-testing-with-webpack/
//
//

const specFile = './tmpWebpackFiles/test-specs.js';       // compiled test-specs.ts

//var webpackConfig = require('./webpack.config.js');   // this is how we would use the pre-existing webpack.config file instead of adding our own config below (which is what we are currently doing)
//webpackConfig.entry = {};   // zap entry


module.exports = function(config) {
    config.set({

        browserNoActivityTimeout: 60000,

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        plugins: [
            'karma-jasmine',
            //'karma-chrome-launcher',
            'karma-phantomjs-launcher',
            'karma-webpack',
        ],

        // list of files / patterns to load in the browser
        files: [
            //specBundle  // use this if webpack is pre-bundling
            specFile
        ],


        // list of files to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            [specFile]: ['webpack']
        },
        //webpack: webpackConfig,
        webpack: {
            entry: specFile,
            resolve: {
                extensions: ['', '.js']
            },
            module: {}
        },
        webpackMiddleware: {
            noInfo: true        // prevent console logging of every file that is processed by webpack
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress'],


        // web server port
        port: 3008, // 9876,	// G.C. NOTE: if port is being used, it will just hang with browser open


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        client:
        {
            captureConsole: false   // we don't want all the console log messages to appear in the command prompt
        },


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],
        //browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        // NOTE. TO DEBUG, SET TO false AND THEN CLICK THE DEBUG BUTTON IN THE BROWSER. ALSO ENSURE THAT THESE SETTINGS ARE SET:
        // plugins: ['karma-chrome-launcher', ...]
        // browsers: ['Chrome'],
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    })
}
