package sample.services;

import org.apache.log4j.Logger;
import sample.entities.Account;
import sample.entities.TradingEntity;

import java.util.*;

/**
 * Created by csandru on 11/4/2016.
 */
public class TradingEntityService {
    private List<TradingEntity> tradingEntities = new ArrayList<TradingEntity>();
    private Map<String, TradingEntity> tradingEntitiesOnIds = new HashMap<>();
    private static int count = 1;



    public TradingEntityService(AccountsService accountsService){
        //populate few trading entities
        TradingEntity te = new TradingEntity();
        te.setName("First");
        Collection<Account> accounts = accountsService.getAllAccounts();
        te.addAccount(accounts.iterator().next());
        addTradingEntity(te);

        te = new TradingEntity();
        te.setName("Second");
        addTradingEntity(te);

    }

    public Collection<TradingEntity> getAvailableTradingEntities(){
        return tradingEntities;
    }

    public TradingEntity addTradingEntity(TradingEntity tradingEntity){
        tradingEntity.setId(Integer.toString(count++));
        tradingEntities.add(tradingEntity);
        tradingEntitiesOnIds.put(tradingEntity.getId(), tradingEntity);
        return tradingEntity;
    }

    public void deleteTradingEntity(String id){
        TradingEntity te = tradingEntitiesOnIds.remove(id);
        if (te != null) {
            tradingEntities.remove(te);
        }
    }

    public TradingEntity getTradingEntity(String id){
        return tradingEntitiesOnIds.get(id);
    }

    public void addAccountToTradingEntity(String teId, Account account){
        Logger logger = Logger.getLogger(this.getClass());
        logger.debug("adding account");
        TradingEntity te = tradingEntitiesOnIds.get(teId);
        if (te != null){
            logger.debug("added account");
            te.addAccount(account);
        }
    }

    public void deleteAccountFromTradingEntity(String teId, String accountId){
        TradingEntity te = tradingEntitiesOnIds.get(teId);
        if (te != null){
            te.deleteAccount(accountId);
        }
    }


}
