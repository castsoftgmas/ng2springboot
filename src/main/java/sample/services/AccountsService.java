package sample.services;

import sample.entities.Account;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by csandru on 11/7/2016.
 */
public class AccountsService {

    public Collection<Account> accounts = new ArrayList<>();

    public AccountsService(){
        Account account = new Account();
        account.setId("1");
        account.setName("Account 1");
        accounts.add(account);
        account = new Account();
        account.setId("2");
        account.setName("Account 2");
        accounts.add(account);
    }

    public Collection<Account> getAllAccounts(){
        return accounts;
    }

    public Account getAccount(String id){
        Iterator<Account> it = accounts.iterator();
        while(it.hasNext()){
            Account account = it.next();
            if (account.getId().equals(id)){
                return account;
            }
        }
        return null;
    }

}
