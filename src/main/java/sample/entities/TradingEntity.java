package sample.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by csandru on 11/4/2016.
 */
public class TradingEntity {

    private String id;
    private String name;
    private String displayName;
    private String description;
    private TradingEntityStatus status;
    private List<Account> accounts = new ArrayList<>();
    private List<TradingEntity> counterparties = new ArrayList<>();



    public TradingEntity(){
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<TradingEntity> getCounterparties() {
        return counterparties;
    }

    public void setCounterparties(List<TradingEntity> counterparties) {
        this.counterparties = counterparties;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TradingEntityStatus getStatus() {
        return status;
    }

    public void setStatus(TradingEntityStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addAccount(Account account){
        //to do test of existance
        accounts.add(account);
    }

    public void deleteAccount(String accountId){
        Iterator<Account> it = accounts.iterator();
        while(it.hasNext()){
            if (it.next().getId().equals(accountId)){
                it.remove();
            }
        }
    }


    public void addCounterparty(TradingEntity counterparty){
        //todo test existence
        counterparties.add(counterparty);
    }

    public void deleteCounterparty(String counterpartyId){
        Iterator<TradingEntity> it = counterparties.iterator();
        while(it.hasNext()){
            if (it.next().getId().equals(counterpartyId)){
                it.remove();
            }
        }
    }

    public void copyFrom(TradingEntity source){
        id = source.id;
        name = source.name;
        description = source.description;
        status = source.status;
        accounts = source.accounts;
        counterparties = source.counterparties;
    }
}
