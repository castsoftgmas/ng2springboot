package sample.entities;

/**
 * Created by csandru on 11/4/2016.
 */
public enum TradingEntityStatus {
    APPROVED,
    SUSPENDED,
    PENDING
}
