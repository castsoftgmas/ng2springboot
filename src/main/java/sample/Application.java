package sample;


import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import sample.services.AccountsService;
import sample.services.TradingEntityService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    @Bean
    public TradingEntityService tradingEntitiesService(){
        return new TradingEntityService(accountsService());
    }

    @Bean
    public AccountsService accountsService(){
        return new AccountsService();
    }


    @Bean
    public FilterRegistrationBean loggingFilterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        Filter userFilter = logFilter();
        registrationBean.setFilter(userFilter);
        registrationBean.setOrder(1);
        return registrationBean;
    }


    @Bean
    public Filter logFilter(){
        return new LogFilter();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }


}