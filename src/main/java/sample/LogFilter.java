package sample;

import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by csandru on 11/7/2016.
 */
class LogFilter implements Filter {
    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long start = System.currentTimeMillis();
        String request = buildRequestString(servletRequest);
        logger.info(">>>START: " + request);
        filterChain.doFilter(servletRequest, servletResponse);
        String response = buildResponseString(servletResponse);
        logger.info(">>>END: " + request + " duration: " + (System.currentTimeMillis() - start) + " ms");
        logger.info(">>>RESPONSE " + response);
    }

    @Override
    public void destroy() {

    }

    private String buildResponseString(ServletResponse servletResponse){
        StringBuilder builder = new StringBuilder();
        builder.append(servletResponse.getContentType());
        return builder.toString();
    }

    private String buildRequestString(ServletRequest servletRequest){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        StringBuilder builder = new StringBuilder();
//            builder.append(servletRequest.getRemoteHost());
//            builder.append(", ");
//            builder.append(servletRequest.getScheme());
//            builder.append(", ");
//            for (Map.Entry<String, String[]> parameter : servletRequest.getParameterMap().entrySet()){
//                builder.append(parameter.getKey());
//                builder.append(": [");
//                for (String param : parameter.getValue()){
//                    builder.append(param + ", ");
//                }
//                builder.append("], ");
//
//            }
//
//            Enumeration<String> attributeNames = servletRequest.getAttributeNames();
//            while (attributeNames.hasMoreElements()){
//                String attribute = attributeNames.nextElement();
//                builder.append(attribute);
//                builder.append(", ");
//            }
        builder.append(request.getMethod() + " ");
        builder.append(request.getRequestURL());
        //builder.append("/");
        //builder.append(request.getRequestURI());
//            Enumeration<String>  headerNames = request.getHeaderNames();
//            while (headerNames.hasMoreElements()) {
//                String headerName = headerNames.nextElement();
//                builder.append("Header name: " + headerName + "[ ");
//                Enumeration<String> attributeNames = request.getHeaders(headerName);
//                while (attributeNames.hasMoreElements()) {
//                    String attribute = attributeNames.nextElement();
//                    builder.append(attribute);
//                    builder.append(", ");
//                }
//                builder.append("], \n");
//            }
        return builder.toString();
    }
}