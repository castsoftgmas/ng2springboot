package sample.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sample.entities.Account;
import sample.entities.TradingEntity;
import sample.services.AccountsService;
import sample.services.TradingEntityService;

import java.util.Collection;

@RestController
@RequestMapping("/{userId}/tradingentities")
public class TradingEntitiesRestController {

    private final TradingEntityService tes;
    private final AccountsService accountsService;

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> add(@PathVariable String userId, @RequestBody TradingEntity input) {
        TradingEntity te = new TradingEntity();
        te.copyFrom(input);

        te = tes.addTradingEntity(te);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(te.getId()).toUri());
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{teId}/accounts/{accountId}", method = RequestMethod.PUT)
    ResponseEntity<?> addAccount(@PathVariable String userId, @PathVariable String teId, @PathVariable String accountId) {
        TradingEntity te = tes.getTradingEntity(teId);
        Account account = accountsService.getAccount(accountId);
        tes.addAccountToTradingEntity(teId, account);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(te.getId()).toUri());
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{teId}/accounts/{accountId}", method = RequestMethod.DELETE)
    ResponseEntity<?>  deleteAccount(@PathVariable String userId, @PathVariable String teId, @PathVariable String accountId) {
        tes.deleteAccountFromTradingEntity(teId, accountId);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(teId).toUri());
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{teId}", method = RequestMethod.GET) //{userId}/tradingentities/{teId}
    TradingEntity get(@PathVariable String userId, @PathVariable String teId) {
        Logger logger = Logger.getLogger(this.getClass());
        logger.debug("TE id " + teId);

        return this.tes.getTradingEntity(teId);
    }

    @RequestMapping(value = "/{teId}", method = RequestMethod.DELETE) //{userId}/tradingentities/{teId}
    void delete(@PathVariable String userId, @PathVariable String teId) {
        Logger logger = Logger.getLogger(this.getClass());
        logger.debug("TE id " + teId);

        this.tes.deleteTradingEntity(teId);
    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<TradingEntity> getAll(@PathVariable String userId) {
        //this.validateUser(userId);
        return tes.getAvailableTradingEntities();
    }

    @Autowired
    TradingEntitiesRestController(TradingEntityService tes, AccountsService accountsService) {
        this.tes = tes;
        this.accountsService = accountsService;
    }


}
