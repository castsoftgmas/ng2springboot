package sample.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sample.entities.Account;
import sample.services.AccountsService;

import java.util.Collection;

/**
 * Created by csandru on 11/7/2016.
 */

@RestController
@RequestMapping("/{userId}/accounts")
public class AccountsRestController {

    private final AccountsService accountsService;

    @Autowired
    AccountsRestController(AccountsService accountsService){
        this.accountsService = accountsService;

    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<Account> getAll(@PathVariable String userId) {
        //this.validateUser(userId);
        return accountsService.getAllAccounts();
    }

}
